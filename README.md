# What is Evexa
Evexa (EVents, EXpenses, Accounting) is a django-website for managing a community's shared economy.

It is designed specifically for the needs of k1200@Prof.Ostenfeld Kollegiet.

In the above regard, it is very similar to [Dinto](https://din.to). However, the key difference is that this is intended to be open source and self-hosted.
The community has the advantage of complete control and cheaper price, but at the cost of having to maintain (keep updated) a VPS or other hosting method.
Note that domains are almost free and that VPS's are usually about 20DKK/month, if you don't get it for free via a github student offer.

# Installation
Evexa is Python 3 only. It should work with Python >=3.5. As a rule of thumb, Evexa will probably only support the python version in the latest Ubuntu LTS (at least a few months after a new one).

Evexa depends on the packages: `django`, `jinja2`, `django-mptt` and `psycopg2`. Install them with `pip3 install -r requirements.txt`.

## Initial development setup
Run
```
./manage.py migrate
./manage.py createsuperuser
```

This will create a local file `db.sqlite3`, which will be your development database, and a superuser for yourself.
You currently have to create more users through `./manage.py shell` (remember to create an account for them as well).

There is an ExpenseType fixture with an example as to how they might be setup; load them with `./manage.py loaddata expenses/fixtures/example_k1200_expensetypes.json` (will overwrite existing data!).

## Launch the development server
You run the django built-in local server. Use it for local development only!
```
./manage.py runserver
```


# Evexa's Aims (not updated to match recent development)
Evexa is under development. The below functionality is what is "needed" functionality (from the need-want-nice2have planning scheme).

## Manage debts to and from community members
Each community member owes (or is owed by) the community some amount of money.

+ Expenses: When a member spends money on behalf of the community (eg. more soap), the member's balance is increased
+ Events: When a member organizes an event, for example a dinner, that member's balance is increased and participating members' balance decreased

## Expense functionality
Everyone can type in an expense that they have had. The community manager can approve it, which will effectuate a transaction to the user.

A future "want" functionality is that the expenses must refer to one of a selection of expense_items having expense_item_aliases.

## Event functionality
Everyone can create and sign up to events. Organizers can specify what they spent on the event, and participants can mark that they brought guests.
Thus, events are "zero-sum" from the perspective of the community.

## Finance
Membership fees decrease members' balances, effectively behaving as a fixed value tax.

Transactions corresponding to cash withdraws/deposits can also be created, which decrease/increase the member's balance.
These transactions should only be used to represent a cash change, if a member has paid eg. 100kr to the community treasury.

The transactions corresponding to withdraws/deposits but without an implied cash transfer are member_donation/debt_waiver, which decrease/increase a member's balance.
If a member has donated money in cash out of the good of his heart, the appropriate structure would thus be a deposit followed by a member_donation.

## Summaries
Want functionality: It should be possible to generate summaries of the last eg. month's expenses, events and so on.

Need functionality: Export in spreadsheet-readable format eg. csv. May be replaced/extended by the wanted summarize functionality later.
