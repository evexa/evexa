from django.contrib import admin
from expenses.models import Expense, ExpenseRefund
# Register your models here.
admin.site.register(Expense)
admin.site.register(ExpenseRefund)
