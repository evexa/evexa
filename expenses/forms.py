from django import forms
from .models import Expense
from evexa.formtools import String_Of_Csv


class ExpenseForm(forms.Form):
    date = forms.DateField()
    quantity = forms.CharField()
    product = forms.CharField()
    price = forms.CharField()
    comment = forms.CharField(required=False)

    def create_expense_for_user(self, user):
        data = self.cleaned_data
        expense_date = data['date']
        quantity = data['quantity']
        product = data['product'].strip().capitalize()
        price = data['price']
        comment = data['comment']
        Expense(name=product, expense_date=expense_date,
                owner=user, quantity=quantity,
                amount=price, comment=comment).save()


class ExpenseManageForm(forms.Form):
    selected_expenses = String_Of_Csv(forms.ModelMultipleChoiceField)(
        queryset=Expense.objects.order_by("-creation_date").prefetch_related("owner"),
    )
    action = forms.ChoiceField(choices=[("A", "Accept"), ("D", "Disapprove"), ("X", "Delete")])

    actionmapping = {"A": lambda e: e.approve(), "D": lambda e: e.disapprove(),
                     "X": lambda e: e.delete()}

    def perform_action(self):
        data = self.cleaned_data
        action = self.actionmapping[data["action"]]
        for e in data["selected_expenses"]:
            action(e)
