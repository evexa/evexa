var selected = [];
$('#select_all').on('change', function() {
	if (this.checked) {
		$('#expenses .pending_cb').each(function(check) {
			this.checked = true;
			setCheckbox(this);
		});
	} else {
		$('#expenses .pending_cb').each(function(check) {
			this.checked = false;
			setCheckbox(this);
		});
	}
});

// for the expense management form
$('#expenses .expense_cb').on('change', function() {
	setCheckbox(this);
});

var setCheckbox = function(cb) {
	if (cb.checked) {
		selected.push(parseInt($(cb).attr('name')));
	} else {
		// remove the expense ID from the list
		selected.splice(selected.indexOf(parseInt($(cb).attr('name'))));
	}
	console.log(selected);
	$('#expense_pks').val(selected)
}

var substringMatcher = function(strs) {
	return function findMatches(q, cb) {
		var matches, substringRegex;

		// an array that will be populated with substring matches
		matches = [];

		// regex used to determine if a string contains the substring `q`
		substrRegex = new RegExp(q, 'i');

		// iterate through the pool of strings and for any string that
		// contains the substring `q`, add it to the `matches` array
		$.each(strs, function(i, str) {
			if (substrRegex.test(str)) {
				matches.push(str);
			}
		});

		cb(matches);
	};
};

$('#typeahead .typeahead').typeahead({
	hint: true,
	highlight: true,
	minLength: 1,
	classNames: {
		input: 'form-control',
		suggestion: 'dropdown-item',
		open: 'dropdown-menu show',
		hint: 'text-hide',
	}
},
	{
		name: 'autocomplete_strings',
    {# TODO: use some asyncstuff connecting to a rest endpoint (https://stackoverflow.com/questions/35451319/typeahead-change-source) #}
		source: substringMatcher({{ autocomplete_strings | safe }})
	});

