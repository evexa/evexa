import datetime
from django.db import models, transaction
from django.db.models import F, Subquery, OuterRef
from django.conf import settings
from finance.models import MoneyTransaction
from mptt.models import MPTTModel, TreeForeignKey, TreeManager


class ExpenseTypeManager(TreeManager):
    def suggestibles(self):
        return self.filter(expense_class__in=ExpenseType.suggestible_values)

    def can_have_expenses(self):
        """Return everything that isnt a category"""
        et = ExpenseType
        return self.filter(expense_class__in=[et.ITEM, et.ALIAS, et.MISSPELLING])

    def annotate_all_time_sum(self):
        """Adds a sum_price field which is the sum of expenses' total_price.
        Also, see accumulate_childrens_value, you may want to call that after"""
        expenses = Expense.objects.filter(expense_type=OuterRef("pk")).aggregate(
            sum_price=models.Sum("total_price")
        ).order_by().values("sum_price")
        return self.annotate(all_time_sum=Subquery(expenses))

    def accumulate_childrens_value(self, to_field, agg_field):
        """ Sum over childrens' (including self) value in agg_field, saving the sum into to_field"""
        sumofchildren = ExpenseType.objects.filter(
            lft__gte=OuterRef("lft"), rght__lte=OuterRef("rght"), tree_id=OuterRef("tree_id")
            ).aggregate(**{to_field: models.Sum(agg_field)}).order_by().values(to_field)
        return self.annotate(**{to_field: Subquery(sumofchildren)})

    # TODO: method for sorting according to how often an alias' items
    # def by_use_frequency(self):
    #    return self.order_by()


class ExpenseType(MPTTModel):
    """Tree for representing categories and expense types and misspellings.
    It is really bad for insertion (which is hopefully not so relevant), but really great for
    fetching, as it doesn't require joins or anything.
    See the docs at https://django-mptt.readthedocs.io/en/latest/index.html
    lft, rght, tree_index and level are automatically created int fields and the parent field is
    required. See the docs for what it means.
    We use categories for aggregation only, items and aliases for suggestions, and misspellings for
    retards (*and bouillon).
    So categories cant have expenses associated with them (only enforced at expense save method)
    Note that the expense_class in is sort of like the "level" field, except there can be multiple
    categories. And i suppose there might sometimes be a sensible structure with sub-aliasing"""
    name = models.CharField(max_length=64, unique=True)
    parent = TreeForeignKey("self", null=True, blank=True, related_name="children", db_index=True)
    CATEGORY = "C"
    ITEM = "I"
    ALIAS = "A"
    MISSPELLING = "M"
    suggestible_values = [ITEM, ALIAS]
    naming_values = [CATEGORY, ITEM, ALIAS]
    expense_class = models.CharField(max_length=1, choices=(
        (CATEGORY, "Category"),
        (ITEM, "Item"),
        (ALIAS, "Alias"),
        (MISSPELLING, "Misspelling")
    ), default=ITEM)

    objects = ExpenseTypeManager()


class ExpenseManager(models.Manager):
    def get_queryset(self):
        """Ensure that expense querysets are always annotated with name and total price"""
        # get the name of the lowest parent (incl. self) that can give name to expenses
        bottom_parent_name = ExpenseType.objects.filter(
            lft__lte=OuterRef("expense_type__lft"), rght__gte=OuterRef("expense_type__rght"),
            tree_id=OuterRef("expense_type__tree_id")
        ).filter(expense_class__in=ExpenseType.naming_values).order_by("-level").values("name")[:1]

        # return that shit
        return super(ExpenseManager, self).get_queryset().annotate(
            total_price=models.ExpressionWrapper(
                F("quantity") * F("amount"),
                output_field=models.DecimalField(decimal_places=2, max_digits=8)),
            name=Subquery(bottom_parent_name)
        )


class Expense(models.Model):
    # name = models.CharField(max_length=254)
    expense_type = models.ForeignKey(ExpenseType, on_delete=models.SET_NULL, null=True)
    expense_date = models.DateField()
    creation_date = models.DateTimeField(default=datetime.datetime.now)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=True,
                              on_delete=models.SET_NULL)
    quantity = models.IntegerField()
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    comment = models.TextField(blank=True, default="")
    AWAITS_APPROVAL = "W"
    DISAPPROVED = "D"
    APPROVED = "A"
    status = models.CharField(max_length=1, choices=(
            (AWAITS_APPROVAL, "Awaits approval"),
            (DISAPPROVED, "Disapproved"),
            (APPROVED, "Approved")
            ), default=AWAITS_APPROVAL)

    objects = ExpenseManager()

    def __init__(self, *args, **kwargs):
        if "name" in kwargs:
            # Roll with it... (django will throw otherwise)
            self.name = kwargs["name"]
            del kwargs["name"]
        super(Expense, self).__init__(*args, **kwargs)

    @property
    def is_approved(self):
        return self.status == self.APPROVED

    @property
    def is_disapproved(self):
        return self.status == self.DISAPPROVED

    @property
    def is_awaiting_approval(self):
        return self.status == self.AWAITS_APPROVAL

    def approve(self):
        if self.status == self.APPROVED:
            return
        elif self.status in (self.DISAPPROVED, self.AWAITS_APPROVAL):
            with transaction.atomic():
                ExpenseRefund(expense=self, balance_delta=self.amount*self.quantity,
                              account=self.owner.account).save()
                self.status = self.APPROVED
                self.save()

    def disapprove(self):
        if self.status == self.DISAPPROVED:
            return
        elif self.status == self.AWAITS_APPROVAL:
            self.status = self.DISAPPROVED
            self.save()
        elif self.status == self.APPROVED:
            with transaction.atomic():
                ExpenseRefund(expense=self, balance_delta=-self.amount*self.quantity,
                              account=self.owner.account).save()
                self.status = self.DISAPPROVED
                self.save()

    def save(self):
        # TODO: something about signals being the right way to do this stuff rather than overloading
        # the save method...
        with transaction.atomic():
            if not self.pk and not self.expense_type:
                if not self.name:
                    raise ValueError("whoops, no name? really?")
                try:
                    etype = ExpenseType.objects.can_have_expenses().get(name=self.name)
                except ExpenseType.DoesNotExist:
                    # TODO: catch the event that name is not unique which will throw here
                    # for example if the expense is named after a category, it will fail.
                    etype = ExpenseType(name=self.name, parent=None)
                    etype.save()
                self.expense_type = etype
            super(Expense, self).save()

    def __str__(self):
        # TODO: return the proper item instead of misspelling/alias
        return self.expense_type.name


class ExpenseRefund(MoneyTransaction):
    # There can be multiple refunds for an expense if it was approved and later disapproved
    # (and so on)
    expense = models.ForeignKey(Expense, null=True, on_delete=models.SET_NULL)
    _context_long_form = "Expenses"

    def __str__(self):
        return "Expense: {}".format(self.expense)
