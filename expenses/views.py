from django.shortcuts import render, reverse as url
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponseBadRequest
from django.contrib.auth.decorators import login_required
from django.db.models import Case, When, Value, Q, IntegerField

from .models import Expense, ExpenseType
from .forms import ExpenseForm, ExpenseManageForm

import math
from decimal import Decimal

TWOPLACES = Decimal(10) ** -2


def handle_list_forms(request):
    postdata = request.POST

    # XXX Create expense
    if 'submit' in postdata and postdata['submit'] == 'Create':
        create_form = ExpenseForm(request.POST)
        if create_form.is_valid():
            create_form.create_expense_for_user(request.user)
        else:
            # TODO: Show user where he went wrong
            return HttpResponseBadRequest("Malform create expense form")

    # XXX Delete expense
    if 'submit' in postdata and postdata['submit'] == 'Remove':
        try:
            e = Expense.objects.get(pk=postdata['expense_id'])
            if e.is_approved:
                return HttpResponseForbidden("You should not delete approved expenses.")
            if e.owner == request.user or request.user.is_staff:
                e.delete()
            else:
                return HttpResponseForbidden("You don't have permission to do that.")
        except Expense.DoesNotExist:
            return HttpResponseBadRequest("The expense you wanted to delete does not exist")

    # XXX Mark expenses as something
    if 'action' in postdata:
        # Add the manage actions here.
        form = ExpenseManageForm(request.POST)
        if form.is_valid() and request.user.is_staff:
            form.perform_action()
        else:
            return HttpResponseBadRequest("You don't have permission or form was invalid.")

    return HttpResponseRedirect(url("expenses:index"))


@login_required
def list(request):
    if request.method == 'POST':
        # Moved to function to reduce function complexity.
        return handle_list_forms(request)

    # XXX: Render the regular page
    create_form = ExpenseForm()

    page = int(request.GET.get('p', 1))
    page_size = 25
    page_start = (page - 1) * page_size
    page_end = page_start + page_size
    expenses = Expense.objects.annotate(
        priority=Case(
            When(Q(status=Expense.AWAITS_APPROVAL), Value(0)),
            default=Value(1),
            output_field=IntegerField()
        )
    ).order_by("priority", "-expense_date", "-creation_date").prefetch_related("owner")
    num_exp = expenses.count()
    expenses_page = expenses[page_start:page_end] if num_exp > 0 else []
    num_pages = math.ceil(num_exp / int(page_size))

    return render(request, 'expenses.html',
                  {'expenses': expenses_page,
                   'num_pages': num_pages,
                   'at_page': page,
                   'autocomplete_strings': [e.name for e in ExpenseType.objects.suggestibles()],
                   'form': create_form
                   })
