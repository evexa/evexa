from django.conf.urls import url
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.urls import reverse

from users.models import User
from django.http import HttpResponseRedirect

from .forms import UserListForm, UserCreationForm, UserEditForm

import json


@login_required
def index(request):
    return HttpResponse("Hello world, users index!")


def user_list_form(request):
    if request.method == "POST":
        form = UserListForm(request.POST)
        if request.user.is_admin and form.is_valid():
            form.perform_action()


@login_required
def users_index(request):
    user_list_form(request)
    users = User.objects.filter(is_active=True).order_by("nick", "name").prefetch_related("account")
    archived_users = User.objects.filter(is_active=False).order_by("-archive_timestamp", "nick",
                                                                   "name").prefetch_related("account")
    return render(request, 'userlist.html', {'users': users,
                                             'archived_users': archived_users})


def get_user_dinner_schedule(user):
    try:
        dinner_schedule = user.dinnerschedule
    except ObjectDoesNotExist:
        user.create_blank_dinner_schedule()
        dinner_schedule = user.dinnerschedule

    return dinner_schedule

@login_required
def your_profile(request, user_id):
    user = User.objects.get(pk=user_id)
    pwd_msg = ""
    if request.method == 'POST':
        if (request.POST.get("action", "") == "RESET_PASSWORD") and request.user.is_admin:
            password = User.objects.make_random_password()
            user.set_password(password)
            user.save()
            pwd_msg = "You have reset {u}'s password to {p} - remember to tell her!".format(
                u=user, p=password)
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            return HttpResponseRedirect('/users/' + user_id)
    else:
        form = PasswordChangeForm(request.user)

    dinner_schedule = get_user_dinner_schedule(user)
    dinner_schedule_list = dinner_schedule.create_weekday_list()

    if request.user.is_admin:
        edit_user_form = UserEditForm()
        edit_user_form.set_initials(user)
    else:
        edit_user_form = []

    return render(request, 'profile.html', {
        'user_of_page': user,
        'form': form,
        'user_edit_form': edit_user_form,
        "password_reset_message": pwd_msg,
        'dinner_schedule_list': dinner_schedule_list,
        'dinner_schedule_active': dinner_schedule.active
    })


@login_required
def save_dinner_schedule(request, user_id):
    if request.method == 'POST':
        if (request.user.id != int(user_id)) and (not request.user.is_admin):
            return HttpResponseBadRequest("Not allowed.")

        dinner_schedule = json.loads(request.POST['schedule'])
        active = json.loads(request.POST['active'])

        if len(dinner_schedule) != 7:
            return HttpResponseBadRequest("Ooops. Something went wrong your request. Please refresh!")

        user = User.objects.get(pk=user_id)
        user.save_new_dinner_schedule(dinner_schedule, active)
        return HttpResponse('success')

    return HttpResponse('FAIL!!!!!')


@login_required
def create_user(request):
    if (request.method == "POST") and request.user.is_admin:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse("Great success! The new user's password was set to '{}'" \
                                " (without quotes). Remember to tell her.".format(form.password))
        return HttpResponse("Great failure!")
    form = UserCreationForm()
    return render(request, 'user-create.html', {
        "form": form,
    })


@login_required
def edit_user(request, user_id):
    user = User.objects.get(pk=user_id)
    if (request.method == "POST") and request.user.is_admin:
        form = UserEditForm(request.POST)
        if form.is_valid():
            user.name = form.cleaned_data["name"]
            user.nick = form.cleaned_data["nick"]
            user.email = form.cleaned_data["email"]
            user.save()

        else:
            return HttpResponseBadRequest("You don't have permission or form was invalid.")

    url = reverse('users:profile', kwargs={'user_id': user.id})
    return HttpResponseRedirect(url)
