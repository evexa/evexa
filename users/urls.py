from django.conf.urls import url, include

from . import views

app_name = "users"
urlpatterns = [
    url(r'^$', views.users_index, name='index'),
    url(r'^auth/', include('django.contrib.auth.urls')),
    url(r'^(?P<user_id>[0-9]+)/$', views.your_profile, name='profile'),
    url(r'^create/$', views.create_user, name="create"),
    url(r'^(?P<user_id>[0-9]+)/save-dinner-schedule/$', views.save_dinner_schedule, name="save-dinner-schedule"),
    url(r'^(?P<user_id>[0-9]+)/edit_user/$', views.edit_user, name='edit_user')
]
