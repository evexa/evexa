from django.core.management import call_command
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Runs makemigrations for the custom apps which django for some reason likes to ignore for the first run"
    appnames = ["users", "finance", "events", "expenses"]

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **kwargs):
        call_command("makemigrations")
        call_command("makemigrations", *self.appnames)
        self.stdout.write("Initial makemigrations done!")
