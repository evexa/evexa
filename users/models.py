from django.db import models, transaction
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from finance.models import Account
from datetime import datetime
from django.utils import timezone

class UserManager(BaseUserManager):
    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})

    def create_user(self, email, name, password=None):
        """
        Creates and saves a User with the given email, etc
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            name=name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            self.normalize_email(email),
            password=password,
            name=name,
        )
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def get_financial_active_users_at(self, date):
        users = User.objects.all()
        pks = [user.pk for user in users if user.account.balance_at(date) != 0.00]
        return super(UserManager, self).get_queryset().filter(pks__in=pks)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=254)
    nick = models.CharField(max_length=8, blank=True)

    is_active = models.BooleanField(default=True)
    archive_timestamp = models.DateTimeField(default=datetime.max)

    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"

    REQUIRED_FIELDS = ["name"]

    objects = UserManager()

    @property
    def is_admin(self):
        # Simplest possible answer: All admins are staff
        return self.is_superuser

    @property
    def is_staff(self):
        # Simplest possible answer: All admins are staff
        return self.is_superuser

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.nick

    def archive(self):
        if not self.is_active:
            return
        with transaction.atomic():
            self.is_active = False
            self.archive_timestamp = timezone.now()
            self.save()

    def unarchive(self):
        if self.is_active:
            return
        with transaction.atomic():
            self.is_active = True
            self.save()

    @property
    def get_balance_status_ok(self):
        if not self.is_active:
            return self.account.balance == 0.00
        else:
            return self.account.balance > -500.00

    def save(self, *args, **kwargs):
        new = False
        if self.pk is None:
            new = True
        with transaction.atomic():
            super().save(*args, **kwargs)
            if new:
                Account(user=self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def create_blank_dinner_schedule(self):
        dinner_schedule = DinnerSchedule(
            user=self
        )
        dinner_schedule.save()

    def save_new_dinner_schedule(self, new_schedule, active):
        self.dinnerschedule.update(new_schedule, active)


class DinnerSchedule(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    CAN_NOTHING = 0
    CAN_EAT = 1
    CAN_COOK_AND_EAT = 2
    DINNER_CHOICES = (
        (CAN_NOTHING, 'Nothing'),
        (CAN_EAT, 'Eat'),
        (CAN_COOK_AND_EAT, 'Cook'),
    )

    active = models.BooleanField(default=0)
    monday = models.IntegerField(default=CAN_NOTHING,
                                 choices=DINNER_CHOICES
                                 )
    tuesday = models.IntegerField(default=CAN_NOTHING,
                                  choices=DINNER_CHOICES
                                  )
    wednesday = models.IntegerField(default=CAN_NOTHING,
                                    choices=DINNER_CHOICES
                                    )
    thursday = models.IntegerField(default=CAN_NOTHING,
                                   choices=DINNER_CHOICES
                                   )
    friday = models.IntegerField(default=CAN_NOTHING,
                                 choices=DINNER_CHOICES
                                 )
    saturday = models.IntegerField(default=CAN_NOTHING,
                                   choices=DINNER_CHOICES
                                   )
    sunday = models.IntegerField(default=CAN_NOTHING,
                                 choices=DINNER_CHOICES
                                 )

    def update(self, dinner_schedule, active):
        self.active = active
        self.monday = dinner_schedule[0]
        self.tuesday = dinner_schedule[1]
        self.wednesday = dinner_schedule[2]
        self.thursday = dinner_schedule[3]
        self.friday = dinner_schedule[4]
        self.saturday = dinner_schedule[5]
        self.sunday = dinner_schedule[6]

        self.save()

    def create_weekday_list(self):
        return [(self.monday, "monday"),
                (self.tuesday, "tuesday"),
                (self.wednesday, "wednesday"),
                (self.thursday, "thursday"),
                (self.friday, "friday"),
                (self.saturday, "saturday"),
                (self.sunday, "sunday")]

    def get_dinner_schedule_as_list(self):
        return [self.monday, self.tuesday, self.wednesday,
                self.thursday, self.friday, self.saturday, self.sunday]