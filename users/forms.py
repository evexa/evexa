from django import forms
from .models import User


def _make_admin(user):
    user.is_superuser = True
    user.save()


def _unmake_admin(user):
    user.is_superuser = False
    user.save()


class UserListForm(forms.Form):
    selected_users = forms.ModelMultipleChoiceField(required=True, queryset=User.objects.all())

    actions = {
        "archive_users": lambda u: u.archive(),
        "unarchive_users": lambda u: u.unarchive(),
        "make_admin": _make_admin,
        "unmake_admin": _unmake_admin,
    }
    action = forms.ChoiceField(choices=[(c, c) for c in actions.keys()])

    def perform_action(self):
        data = self.cleaned_data
        for user in data["selected_users"]:
            self.actions[data["action"]](user)
        return


class UserCreationForm(forms.ModelForm):
    password = False

    class Meta:
        model = User
        fields = ["email", "name", "nick"]

    def save(self, commit=True):
        instance = super().save(commit=False)
        self.password = User.objects.make_random_password()
        instance.set_password(self.password)
        instance.save()
        return instance


class UserEditForm(forms.Form):
    email = forms.EmailField()
    name = forms.CharField(max_length=254)
    nick = forms.CharField(max_length=8)

    def set_initials(self, user):
        self.fields['email'].initial = user.email
        self.fields['name'].initial = user.name
        self.fields['nick'].initial = user.nick

