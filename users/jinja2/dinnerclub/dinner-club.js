$(document).on("click","#save-dinner-schedule",function() {
    var schedule = [0, 0, 0, 0, 0, 0, 0];
    var days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];

    for (i = 0; i < schedule.length; i++) {
        var eat = false;
        var cook = false;

        $("." + days[i] + ":checkbox:checked").each(function () {

            if ($( this ).attr("id").includes("Eat")) {
                eat = true;
            }

            if ($( this ).attr("id").includes("Cook")) {
                cook = true;
            }

        });

        schedule[i] = map_to_int(eat, cook);
    }

    function map_to_int(eat, cook) {
        if (cook) {
            return 2;
        } else if (eat) {
            return 1;
        } else {
            return 0;
        }
    }

    var is_active = $('#toggle-yes-label').hasClass('active');

    $.ajax({
        url: 'save-dinner-schedule/',
        type: 'post',
        data: {'schedule': JSON.stringify(schedule),
            'active': is_active,
            'csrfmiddlewaretoken': '{{ csrf_token }}'
        },
        success: function() {
            alert("Successfully saved your dinner schedule!");
            re_render_html(is_active);
        },
        error: function() {
            alert("Something went wrong! :(");
        }
    });
});

function re_render_html(active) {
    if (active) {
        $("#dinner-schedule-extension").text("- active");
    } else {
        $("#dinner-schedule-extension").text("- inactive");
        $('#collapse-dinner-schedule').collapse('hide');
    }
}

$(document).on("click", "#toggle-buttons-group", function() {

  if ($('#yes-toggle-btn').is(":checked")) {
      $('#collapse-dinner-schedule').collapse('show');
  }
});

$(document).on("click",".weekDays-selector",function() {
  var days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];

  var isCook = $(this).hasClass("Cook");
  var update = isCook ? "Eat" : "Cook";
  var from = !isCook ? "Eat" : "Cook";

  days.forEach(function(day) {
    var eat = $("#Eat" + day).prop('checked');
    var cook = $("#Cook" + day).prop('checked');
    if (cook & !eat) {
      $("#" + update + day).prop('checked', $("#" + from + day).prop('checked'));
    }
  });
});


$(document).ready(function(){
    if ($('#toggle-yes-label').hasClass('active')) {
        $('#collapse-dinner-schedule').collapse('show')
  }
});
