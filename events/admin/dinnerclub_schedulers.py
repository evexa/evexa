from abc import ABC, abstractmethod
from pulp import LpProblem, LpMinimize, LpVariable, LpInteger, LpBinary, lpSum, LpStatus

from datetime import timedelta, datetime, time
from events.models import Event

from users.models import DinnerSchedule

from evexa.log_tools import SimpleLogger


class BaseScheduler(ABC):

    def __init__(self, start, end, number_each_week, active_users, unavailable_dates):
        """
        Base input from dinner club admin
        """
        self.events = []
        self.start = start
        self.end = end
        self.number_each_week = number_each_week
        self.active_users = list(active_users)
        self.unavailable_dates = unavailable_dates
        self.logger = SimpleLogger()

    def save_events(self):
        for (event, organizers) in self.events:
            event.save()
            event.set_organizing(organizers)
            event.set_participating(organizers)

    def create_event(self, date, organizers):
        date_with_time = datetime.combine(date, time(hour=19, minute=0, second=0, microsecond=0))

        event = Event(name=" - ".join(["Dinner club"] + [organizer.nick for organizer in organizers]),
                      date=date_with_time,
                      automatically_created=True
                      )

        self.events.append((event, organizers))

    @abstractmethod
    def schedule(self):
        pass


class LinearSolverDinnerScheduler(BaseScheduler):

    def __init__(self, **args):
        super().__init__(**args)

        # This solver cannot deal with active users with no cooking days
        # Hence, they are left out
        size = len(self.active_users)
        self.active_users = list(
            filter(
                lambda x: 2 in x.dinnerschedule.get_dinner_schedule_as_list(), self.active_users
            )
        )
        if size != len(self.active_users):
            self.logger.warning("At least one user did not specify any cooking days")

        # Range of residents
        self.residents = range(len(self.active_users))
        # Init users schedules for algorithm
        dinner_schedules = list(DinnerSchedule.objects.filter(user__in=self.active_users).order_by('user__name'))
        dinner_schedules_lists = [dinner_schedule.get_dinner_schedule_as_list() for dinner_schedule in dinner_schedules]
        self.residents_available = dict(zip(list(self.residents), dinner_schedules_lists))

        self.infeasible = False

    def schedule(self):

        ###############
        # MODEL BEGIN #
        ###############

        # Credits for model: Karen Egebaek Nicolajsen
        number_of_days = (self.end - self.start).days + 1
        days = range(number_of_days)

        number_of_weeks = int(number_of_days / 7)
        weeks = range(number_of_weeks)

        number_of_ud = len(self.unavailable_dates)
        unavailable_dates_index = []
        for i in range(0, number_of_ud):
            unavailable_dates_index.append((self.unavailable_dates[i] - self.start).days)

        jobs_per_person = round(number_of_weeks * self.number_each_week * 2 / len(self.active_users))
        interval = round(number_of_days / jobs_per_person)

        # FACTORS TO CONTROL IMPORTANCE
        weight_nr_dinner_clubs_each_weak = len(self.active_users)
        weight_clubs_pr_person = 5
        weight_spread_pr_person = 20
        weight_attendance = 1

        prob = LpProblem("EvexaDinnerClub", LpMinimize)

        ##### VARIABLES #####
        # jobs for persons with min and max, respectively
        min_jobs = LpVariable("MinNoOfJobs", 0, None, LpInteger)
        max_jobs = LpVariable("MaxNoOfJobs", 0, None, LpInteger)

        # deviation from dinner_clubs_each_weak
        max_deviation = LpVariable("MaxDeviationFromM", 0, None, LpInteger)

        # Binary indication of dinnerClub a given day
        dinner_club = LpVariable.dicts("DinnerClubOrNot", days, 0, 1, LpBinary)

        # Binary indication of cooks for a given day
        cooks = LpVariable.dicts("CooksForDinnerClub", [(i, j) for i in self.residents for j in days], 0, 1, LpBinary)

        # People attending dinner clubs
        guests = LpVariable.dicts("GuestsForDinnerClub", days, 0, None, LpInteger)

        # The minimum number of guests for a dinner club
        min_guests = LpVariable("MinimumNumberOfGuests", 0, None, LpInteger)

        # Nr. of additional jobs (1 is assumed) in the calculated interval
        max_exceed = LpVariable("MaxIntervalExceed", 0, None, LpInteger)

        #### OBJECTIVE FUNCTION ####
        prob += weight_nr_dinner_clubs_each_weak * max_deviation + weight_clubs_pr_person * (
                max_jobs - min_jobs) + weight_spread_pr_person * max_exceed - weight_attendance * min_guests

        #### CONSTRAINTS #####
        # Two cooks each day, 0 if no dinnerclub
        for j in days:
            prob += lpSum(cooks[(i, j)] for i in self.residents) == 2 * dinner_club[j]

        # Prevents user from being cook on a non-requested day
        for i in self.residents:
            for j in days:
                prob += 2 * cooks[(i, j)] <= self.residents_available[i][j % 7]
                prob += cooks[(i, j)] <= dinner_club[j]

        # No dinnerclubs on unavailable dates
        for i in unavailable_dates_index:
            prob += dinner_club[i] == 0

        # min/max jobs for each each resident
        for i in self.residents:
            prob += min_jobs <= lpSum(cooks[(i, j)] for j in days)
            prob += max_jobs >= lpSum(cooks[(i, j)] for j in days)

        # Deviation from number of dinner clubs each week
        for i in weeks:
            prob += max_deviation >= self.number_each_week - lpSum(dinner_club[j] for j in range(i * 7, i * 7 + 7))
            prob += max_deviation >= lpSum(dinner_club[j] for j in range(i * 7, i * 7 + 7)) - self.number_each_week

        # All intervals are compared to assure that there is a spread among
        # each users cook dinnerclubs
        for i in self.residents:
            for j in range(number_of_days - interval):
                prob += max_exceed >= lpSum(cooks[(i, j + k)] for k in range(0, interval)) - 1

        # Calculates all guests for each dinner club
        for j in days:
            prob += guests[j] == dinner_club[j] * lpSum(
                round(self.residents_available[i][j % 7] / (self.residents_available[i][j % 7] + 0.01)) for i in
                self.residents)
            prob += min_guests <= guests[j] + len(self.active_users) * (1 - dinner_club[j])

        #### SOLVE ####
        prob.solve()

        #############
        # MODEL END #
        #############

        #### RESULTS ####
        if LpStatus[prob.status] == 'Infeasible':
            self.logger.failure("The problem was infeasible")
            self.infeasible = True
        else:
            for i in days:
                if dinner_club[i].varValue >= 1:
                    dc_date = self.start + timedelta(days=i)
                    organizers = [self.active_users[j] for j in self.residents if cooks[(j, i)].varValue >= 1]
                    self.create_event(dc_date, organizers)

                    attendants = sum((round(
                        self.residents_available[j][i % 7] / (self.residents_available[j][i % 7] + 0.01))
                        for j in self.residents))

                    self.logger.info("Attendants for {0} (including cooks): {1}".format(dc_date, attendants))

            for i in weeks:
                self.logger.info(
                    "Nr. of dinnerclubs week {0}: {1}".format(
                        i, sum(dinner_club[j].varValue for j in range(i * 7, i * 7 + 7))
                    )
                )

            for i in self.residents:
                self.logger.info(
                    "Total dinnerclubs for user {0}: {1}".format(
                        self.active_users[i].get_full_name(), sum(cooks[(i, j)].varValue for j in days )
                    )
                )

            self.logger.info("Objective value: {0}".format(prob.objective))
            self.logger.info("Max. weekly deviation: {0}".format(max_deviation.varValue))
            self.logger.info("Job deviation: {0}".format(max_jobs.varValue - min_jobs.varValue))
            self.logger.info("Max. interval exceed: {0}".format(max_exceed.varValue))
            self.logger.info("Minimum number of guests: {0}".format(min_guests.varValue))
            self.logger.info("Number of jobs: {0}".format(jobs_per_person))
            self.logger.info("Interval: {0}".format(interval))
