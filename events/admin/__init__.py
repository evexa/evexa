# To replace the admin.py file from previously:
from django.contrib import admin
from events.models import Event, EventParticipant, EventSettlement
# Register your models here.
admin.site.register(Event)
admin.site.register(EventParticipant)
admin.site.register(EventSettlement)
