from datetime import date
from django import forms

from events.admin.dinnerclub_schedulers import LinearSolverDinnerScheduler
from events.models import Event


class DeleteEventForm(forms.Form):
    selected_events = forms.ModelMultipleChoiceField(required=True, queryset=Event.objects.all())

    def delete_events(self):
        data = self.cleaned_data
        for event in data["selected_events"]:
            event.delete()
        return


def date_string_to_ints(date_string):
    splitted_string = date_string.split('/')
    return int(splitted_string[0]), int(splitted_string[1]), int(splitted_string[2])


class DinnerClubInputForm(forms.Form):
    start = forms.DateField(input_formats=(['%d/%m/%Y']))
    end = forms.DateField(input_formats=(['%d/%m/%Y']))
    number_each_week = forms.IntegerField()

    def clean(self):
        data = super().clean()
        if data["start"] >= data["end"] or data["start"] < date.today():
            raise ValueError('Period picked wrong.')

        if data["start"].weekday() != 0 and data["end"].weekday() != 6:
            raise ValueError('Start date must be monday and end date must be sunday.')

        string_dates = self.data["unavailable_dates"].split(",")
        unavailable_dates = []

        for date_string in string_dates:
            if not date_string:
                continue
            try:
                day, month, year = date_string_to_ints(date_string)
            except TypeError:
                raise ValueError('Unavailable date format is wrong.')

            unavailable_date = date(year, month, day)

            if unavailable_date < data["start"] or unavailable_date > data["end"]:
                raise ValueError('Unavailable dates not in period.')

            unavailable_dates.append(unavailable_date)

            date_string_to_ints(date_string)

        data["unavailable_dates"] = unavailable_dates

        return data