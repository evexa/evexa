from django.conf.urls import url

from . import views

app_name = "admin"
urlpatterns = [
            url(r'^$', views.admin_page, name='index'),
            url(r'^create/$', views.create_dinner_plan, name='create'),
            url(r'^reset/$', views.reset_dinner_activity, name='reset'),
            url(r'^delete/$', views.delete_auto_dinner_clubs, name='delete'),
            ]
