from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import render, reverse as url

from datetime import date

from events.admin.dinnerclub_schedulers import LinearSolverDinnerScheduler
from users.models import User, DinnerSchedule
from events.models import Event

from .forms import DeleteEventForm, DinnerClubInputForm


@login_required
def admin_page(request):
    users = User.objects.filter(is_active=True).order_by("nick", "name") \
                .select_related("dinnerschedule")[::1]

    week = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    eatcook_cats = {
        "Eat": {DinnerSchedule.CAN_COOK_AND_EAT, DinnerSchedule.CAN_EAT},
        "Cook": {DinnerSchedule.CAN_COOK_AND_EAT, }
    }
    eatcook_sums = {
        cat: {
            day: sum(
                (1 if (getattr(user.dinnerschedule, day) in catset) else 0)
                for user in users if hasattr(user, 'dinnerschedule')
            )
            for day in week
        }
        for cat, catset in eatcook_cats.items()
    }

    events = Event.objects.filter(automatically_created=True, date__gte=date.today()).exclude(
        state=Event.CLOSED).order_by("date")

    return render(request, 'admin.html',
                  {
                      'users': users,
                      'events': events,
                      'eatcook_sums': eatcook_sums
                  }
                  )


@user_passes_test(lambda u: u.is_admin)
def create_dinner_plan(request):
    if request.method == "POST":
        form = DinnerClubInputForm(request.POST)
        if form.is_valid():
            form.cleaned_data["active_users"] = get_active_dinner_users()
            scheduler = LinearSolverDinnerScheduler(**form.cleaned_data)
            scheduler.schedule()
            scheduler.save_events()

            return HttpResponseRedirect(url("events:admin:index"))

    return HttpResponseBadRequest("Wrong usage of request!")


def get_active_dinner_users():
    return User.objects.filter(is_active=True, dinnerschedule__active=True).order_by("name")


@user_passes_test(lambda u: u.is_admin)
def reset_dinner_activity(request):
    if request.method == "POST":
        active_users = list(get_active_dinner_users())

        for user in active_users:
            user.dinnerschedule.active = False
            user.dinnerschedule.save()

        return HttpResponseRedirect(url("events:admin:index"))

    return HttpResponseBadRequest('Wrong usage of request')


@user_passes_test(lambda u: u.is_admin)
def delete_auto_dinner_clubs(request):
    if request.method == "POST":
        form = DeleteEventForm(request.POST)
        if form.is_valid():
            form.delete_events()

        return HttpResponseRedirect(url("events:admin:index"))

    return HttpResponseBadRequest('Wrong usage of request')
