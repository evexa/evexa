$(function () {
	$('#datepicker').datetimepicker({
		locale: 'da',
		format: 'L'
	});
});

$(function () {
	$('#timepicker').datetimepicker({
		locale: 'da',
		format: 'LT'
	});
});

$(".userselect div div label").click(function(){
	{# TODO: Make this work properly.... Ie it shoud say "Participants: user1, user2, ... <caret>" #}
	$(this).parents(".userselect").find('.dropdown-toggle').text($(this).text());
	$(this).parents(".userselect").find('.dropdown-toggle').val($(this).text());

});

var selected = []
{% if event %}{% for o in event.organizer_set %}
selected.push({{o.user.pk}});
{% endfor %}{% endif %}
$('#id_organizers').val(selected);

$('#id_organizers').selectize({
	plugins: ['remove_button'],
	delimiter: ',',
	persist: false,
	create: function(input) {
		return {
			value: input,
			text: input
		}
	}
});
