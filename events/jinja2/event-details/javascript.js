$('#guestModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var name = button.data('name');
	var pk = button.data('pk');
	var guests = button.data('guests');
	var modal = $(this);
	modal.find('.modal-title').text('Set no. of guests for ' + name);
	modal.find('.modal-body #id_num_guests').val(Math.trunc(guests));
	modal.find('.modal-body #id_user').val(pk);
	modal.find('form').on('submit', function() {
		// increment number of guests before submission
		modal.find('.modal-body #id_num_guests').val(function(i, oldval) {
			return ++oldval;
		});
	});
});

$('#expenseModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var name = button.data('name');
	var pk = button.data('pk');
	var expenses = button.data('expenses');
	var modal = $(this);
	modal.find('.modal-title').text('Set expenses for ' + name);
	modal.find('.modal-body #id_expenses').val(expenses);
	modal.find('.modal-body #id_user').val(pk);
});
