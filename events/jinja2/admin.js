$(document).ready(function () {
    $('#from').datetimepicker({
        format: 'L',
        locale: 'da',
        daysOfWeekDisabled: [0, 2, 3, 4, 5, 6],
        calendarWeeks: true
    });

    $('#to').datetimepicker({
        format: 'L',
        locale: 'da',
        daysOfWeekDisabled: [1, 2, 3, 4, 5, 6],
        calendarWeeks: true
    });

    // https://github.com/tempusdominus/bootstrap-4/issues/168 allowMultidate not working...
    $('#unavailabledates').datetimepicker({
        allowMultidate: true,
        multidateSeparator: ','
    });

    $('#selectallevents').change(function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
});
