from django.conf.urls import url, include

from . import views

app_name = "events"
urlpatterns = [
            url(r'^$', views.list_events, name='index'),
            url(r"^(?P<year>\d+)/(?P<month>\d+)$", views.list_events, name="month"),
            url(r'^create$', views.create, name='create'),
            url(r'^(?P<event_id>[0-9]+)/$', views.details, name='details'),
            url(r'^(?P<event_id>[0-9]+)/edit/$', views.edit, name='edit'),
            url(r'^admin/', include('events.admin.urls')),
            ]
