from django import forms
from django.utils.translation import activate

from users.models import User
from .models import Event

activate('dk')


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        exclude = ['state', 'related_users']

    organizers = forms.ModelMultipleChoiceField(required=False,
                                                queryset=User.objects.filter(is_active=True))
    name = forms.CharField(required=False,
                           widget=forms.TextInput(attrs={
                                        'class': 'form-control'}))
    date = forms.SplitDateTimeField(input_date_formats=(['%d/%m/%Y']))
    description = forms.CharField(required=False, label='Description',
                                  widget=forms.Textarea(attrs={'class': 'form-control',
                                         'placeholder': 'Description'}))

    def save(self, commit=True, *args, **kwargs):
        # print(self.instance.state)
        if self.instance.state != self.instance.OPEN:
            # Todo: Should be caught in validation of some sort
            raise Exception("You cannot edit a closed event.")

        event = super(self.__class__, self).save()

        # Set organizers as both organizers and participants
        event.set_organizing(self.cleaned_data['organizers'])
        event.set_participating(self.cleaned_data["organizers"])

        # set all organizers after saving the event
        to_remove = event.organizer_set.exclude(user__in=self.cleaned_data['organizers']).values("user")
        event.unset_organizing(to_remove)

        return event


class SetExpenseForm(forms.Form):
    user = forms.ModelChoiceField(User.objects, widget=forms.HiddenInput())
    expenses = forms.CharField()


class SetGuestsForm(forms.Form):
    user = forms.ModelChoiceField(User.objects, widget=forms.HiddenInput())
    num_guests = forms.CharField()
