from django.db import models
from django.db.models import Subquery, Exists, OuterRef, Sum
from django.db.models import Q, F, Value, CharField, Case, When, OuterRef, Subquery, Sum
from django.conf import settings
from django.db import transaction
from django.utils.functional import cached_property
from finance.models import MoneyTransaction, balancefield
from users.models import User

from decimal import Decimal

TWOPLACES = Decimal("10") ** -2


class EventManager(models.Manager):
    def get_queryset(self):
        qset = super().get_queryset()
        participants = Subquery(EventParticipant.objects.filter(
            event=OuterRef("pk"), role=EventParticipant.PARTICIPANT
        ).order_by().values("event").annotate(total=Sum("share")).values("total")[:1],
                                output_field=balancefield())
        organizers = Subquery(EventParticipant.objects.filter(
            event=OuterRef("pk"), role=EventParticipant.ORGANIZER
        ).order_by().values("event").annotate(total=Sum("share")).values("total")[:1],
                              output_field=balancefield())
        qset = qset.annotate(total_shares=participants, total_expenses=organizers)
        return qset

    def with_roles_for(self, user):
        is_participant = Exists(EventParticipant.objects.filter(
            event=OuterRef("pk"), user=user, role=EventParticipant.PARTICIPANT))
        is_organizer = Exists(EventParticipant.objects.filter(
            event=OuterRef("pk"), user=user, role=EventParticipant.ORGANIZER))
        return self.get_queryset().annotate(
            # Todo -- find better names (close to conflict with is_participating and org.)
            is_participant=is_participant,
            is_organizer=is_organizer
        )


class Event(models.Model):
    name = models.CharField(max_length=255)
    date = models.DateTimeField()
    description = models.TextField(blank=True)
    related_users = models.ManyToManyField(User, through="EventParticipant")
    automatically_created = models.BooleanField(default=False)

    OPEN = "O"
    LOCKED = "L"
    CLOSED = "C"
    state = models.CharField(max_length=1, choices=(
            (OPEN, "Open"),
            (LOCKED, "Locked"),
            (CLOSED, "Closed")
            ), default=OPEN)

    objects = EventManager()

    @property
    def participant_set(self):
        return self.eventparticipant_set.filter(role=EventParticipant.PARTICIPANT)

    @property
    def organizer_set(self):
        return self.eventparticipant_set.filter(role=EventParticipant.ORGANIZER)

    @cached_property
    def participants(self):
        return list(self.participant_set)

    @cached_property
    def organizers(self):
        return list(self.organizer_set)

    def is_participating(self, user, cached=True):
        if cached:
            return any(user == p.user for p in self.participants)
        return self.participant_set.filter(user=user).exists()

    def is_organizing(self, user, cached=True):
        if cached:
            return any(user == o.user for o in self.organizers)
        return self.organizer_set.filter(user=user).exists()

    def __str__(self):
        return self.name

    def _invalidate_cached_properties(self):
        """Call this when changing participants or organizers."""
        to_invalidate = ["participants", "organizers"]
        for field in to_invalidate:
            self.__dict__.pop(field, None)

    def _set_single_user_role(self, user, role, share, update_share=True):
        self._invalidate_cached_properties()
        try:
            ep = self.eventparticipant_set.get(user=user, role=role)
            if update_share:
                ep.share = share
        except EventParticipant.DoesNotExist:
            ep = EventParticipant(user=user, role=role, event=self)
            ep.share = share
        ep.save()

    def _unset_single_user_role(self, user, role):
        self._invalidate_cached_properties()
        try:
            ep = self.eventparticipant_set.get(user=user, role=role)
        except EventParticipant.DoesNotExist:
            return
        ep.delete()

    def _set_role(self, users, role, share=None, default_share=0):
        """See set_organizing and set_participating."""
        if share is None:
            share = default_share
            update_share = False
        else:
            update_share = True

        # TODO: We perform DB-lookup in a loop. Should not be so, obviously.
        try:
            for user in users:
                self._set_single_user_role(user, role, share, update_share=update_share)
        except TypeError:  # when users is not iterable
            self._set_single_user_role(users, role, share, update_share=True)

    def _unset_role(self, users, role):
        # TODO: We perform DB-lookup in a loop. Should not be so, obviously.
        try:
            for user in users:
                self._unset_single_user_role(user, role)
        except TypeError:  # when users is not iterable
            self._unset_single_user_role(users, role)

    def set_organizing(self, users, share=None):
        """Set the users (iterable or single user object) as organizers on this event.

        The default share [ie. expense] is 0.
        If share is given, it is updated on user[s]. Otherwise,
        - if users is an iterable, share is not updated on users already marked as organizers.
        - if users is single user object, it is set to zero regardless if user is already organizin.
        """
        self._set_role(users, EventParticipant.ORGANIZER, share=share, default_share=0)

    def set_participating(self, users, share=None):
        """Set the users (iterable or single user object) as participants on this event.

        The default share [eg. self+guests] is 1.
        If share is given, it is updated on user[s]. Otherwise,
        - if users is an iterable, share is not updated on users already marked as participants.
        - if users is single user object, it is set to 1 regardless if user is already participatin.
        """
        self._set_role(users, EventParticipant.PARTICIPANT, share=share, default_share=1)

    def unset_organizing(self, users):
        """The opposite of set_organizing."""
        self._unset_role(users, EventParticipant.ORGANIZER)

    def unset_participating(self, users):
        """The opposite of set_participating."""
        self._unset_role(users, EventParticipant.PARTICIPANT)

    @property
    def is_closed(self):
        """For easy bool checking in templates"""
        return self.state == self.CLOSED

    @property
    def is_locked(self):
        """For easy bool checking in templates"""
        return self.state == self.LOCKED

    @property
    def is_open(self):
        """For easy bool checking in templates"""
        return self.state == self.OPEN

    @property
    def time_of_day(self):
        """str for use in views"""
        return self.date.strftime("%H:%M")

    @cached_property
    def organizer_expenses(self):
        total = self.organizer_set.aggregate(models.Sum("share"))["share__sum"]
        if total is None:
            total = 0
        return Decimal(total).quantize(TWOPLACES)

    @cached_property
    def participant_shares(self):
        # See also total_shares which is being annotated via the manager
        total = self.participant_set.aggregate(models.Sum("share"))["share__sum"]
        if total is None:
            total = 0
        return Decimal(total).quantize(TWOPLACES)

    @cached_property
    def per_share_amount(self):
        shares = self.participant_shares
        if shares == 0:
            return Decimal("0.00")
        return Decimal(self.organizer_expenses/self.participant_shares).quantize(TWOPLACES)

    @property
    def per_share_float(self):
        shares = self.total_shares  # the annotated property
        if shares == 0:
            return 0.0
        return float(self.total_expenses) / float(self.total_shares)

    def _new_settlements(self, reverse=False):
        # Currently results in a lot of db hits. Could be optimized for sure! (but not so important)
        with transaction.atomic():
            price = self.per_share_float * (-1 if reverse else 1)
            for participant in self.participant_set.prefetch_related("user__account"):
                amount = Decimal(- price * float(participant.share)).quantize(TWOPLACES)
                EventSettlement(event=self, balance_delta=amount,
                                    account=participant.user.account, reversal=reverse).save()

            for organizer in self.organizer_set.prefetch_related("user__account"):
                amount = organizer.share * (-1 if reverse else 1)
                EventSettlement(event=self, balance_delta=amount,
                                account=organizer.user.account, reversal=reverse).save()

    def close(self):
        if self.state == self.CLOSED:
            return
        with transaction.atomic():
            self._new_settlements()
            self.state = self.CLOSED
            self.save()

    def open(self):
        if self.state == self.OPEN:
            return
        if self.state == self.CLOSED:
            with transaction.atomic():
                # Revert transactions
                self._new_settlements(reverse=True)
                self.state = self.OPEN
                self.save()
        else:
            self.state = self.OPEN
            self.save()

    def lock(self):
        if self.state == self.LOCKED:
            return
        elif self.state == self.OPEN:
            self.state = self.LOCKED
            self.save()
        elif self.state == self.CLOSED:
            with transaction.atomic():
                # Revert transactions
                self._new_settlements(reverse=True)
                self.state = self.LOCKED
                self.save()

    def delete(self, *args, **kwargs):
        if self.state == self.CLOSED:
            raise Exception("Cannot delete an event that is closed.")
        return super().delete(*args, **kwargs)


class EventParticipant(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    event = models.ForeignKey(Event)
    # the spare property has different meanings depending on the role
    # for an ORGANIZER it is the expenses an organizer have had
    # for a PARTICIPANT it is the number of guests PLUS the participant himself
    #   eg. share==1 for no guests, share==2 for 1 guest...
    share = models.DecimalField(max_digits=8, decimal_places=2)
    ORGANIZER = "O"
    PARTICIPANT = "P"
    role = models.CharField(max_length=1,
                            choices=(
                                    (ORGANIZER, "Organizer"),
                                    (PARTICIPANT, "Participant")
                                    ))

    def __str__(self):
        return str(self.user)


class EventSettlement(MoneyTransaction):
    event = models.ForeignKey(Event, null=True, on_delete=models.SET_NULL)
    reversal = models.BooleanField(null=False, default=False)  # ie. a reopening
    # _context_long_form = "Events"

    @classmethod
    def get_context_long_form(cls):
        qs = cls.objects.filter(moneytransaction_ptr_id=OuterRef("pk")).order_by()

        pm = Case(When(Q(reversal=False), then=1),
                  When(Q(reversal=True), then=-1),
                  output_field=models.IntegerField()
                 )
        pm = F("balance_delta") * pm
        pm.output_field = balancefield()
        qs = qs.annotate(pm=pm)
        ctx = Case(When(Q(pm__lte=0), then=Value("Events(-)")),
                   When(Q(pm__gte=0), then=Value("Events(+)")),
                   output_field=models.CharField()
                   )
        return Subquery(qs.annotate(longform=ctx).values("longform")[:1])


    def __str__(self):
        return "EventSettlement: {}".format(self.event)
