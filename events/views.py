from django.shortcuts import render, reverse as url
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from users.models import User

from .models import Event, EventParticipant
from .forms import EventForm
from .forms import SetExpenseForm, SetGuestsForm

from datetime import date
import datetime
from markdown import markdown


def first_monday_of_month(year, month):
    day = datetime.date(year, month, 1)
    days_ahead = 7 - day.weekday()
    first_monday = day + datetime.timedelta(days_ahead)

    return first_monday


def calendar_window(year, month):
    first_day = first_monday_of_month(year, month) - datetime.timedelta(days=7)
    last_day = first_day + datetime.timedelta(days=42)

    return (first_day, last_day)


@login_required
def list_events(request, year=None, month=None, rolling=False):
    """ A calendar overview of the events """
    # Figure out the date
    today = datetime.date.today()
    yp = 0
    if month is None:
        month = today.month
    else:
        month = int(month)
        yp, month = divmod(month-1, 12)
        month += 1
    if year is None:
        year = today.year
    else:
        year = int(year)
    year += yp

    if rolling:
        # TODO: Handle rolling instead of fixed month view (ie one week back, 4 weeks forward)
        raise NotImplementedError("Evexa does not support a rolling month view yet")

    selected_date = date(year, month, 1)

    begin, _ = calendar_window(year, month)
    dates = [begin + datetime.timedelta(days=x) for x in range(42)]

    # split month into 7 days lists
    day_names = [d.strftime("%A") for d in dates[0:7]]
    dates = [
            {
                'day': d.day,
                'this_month': d.month == selected_date.month,
                'today': d == today,
                'events': [],
                "date": d,
            } for d in dates]

    weeks = [dates[i:i+7] for i in range(0, len(dates), 7)]
    events = Event.objects.with_roles_for(request.user)
    for e in events:
        # find the corresponding date in the dates list
        # e.is_participant = e.is_participating(request.user)
        for d in dates:
            if d['date'] == e.date.date():
                # attach the event to the date element
                d['events'].append(e)

    return render(request, 'events.html', {
        'weeks': weeks, 'day_names': day_names, 'selected_date': selected_date,
        "year": year, "month": month})


@login_required
def create(request):
    if request.method == 'POST':
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()

            return HttpResponseRedirect("/events/")
    else:
        form = EventForm()
        return render(request, 'edit-event.html', {'form': form})


def get_event_by_id(event_id):
    return Event.objects.get(pk=event_id)


@login_required
def edit(request, event_id):
    event = get_event_by_id(event_id)
    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/events/' + str(event_id) + '/')
    else:
        form = EventForm(instance=event)
    return render(request, 'edit-event.html', {'form': form, 'action': 'edit', 'event': event})


@login_required
def details(request, event_id):
    if request.method == 'POST':
        postdata = request.POST.copy()
        # print(postdata)
        if 'submit' in postdata:
            if postdata['submit'] == 'Remove':
                Event.objects.get(pk=event_id).delete()
                return HttpResponseRedirect('/events/')
            if postdata['submit'] == 'Close':
                Event.objects.get(pk=event_id).close()
            if postdata['submit'] == 'Open':
                Event.objects.get(pk=event_id).open()
            if postdata['submit'] == 'Lock':
                Event.objects.get(pk=event_id).lock()
            if postdata['submit'] == 'Cancel':
                # TODO decide if this is necessary
                pass
            if postdata['submit'] == 'SignSelfUp':
                Event.objects.get(pk=event_id).set_participating(request.user)
            if postdata['submit'] == 'SignSelfOut':
                Event.objects.get(pk=event_id).unset_participating(request.user)
            if postdata['submit'] == 'SetExpense':
                user = EventParticipant.objects.get(pk=postdata['user'])
                user.share = postdata['expenses']
                user.save()
            if postdata['submit'] == 'SetGuests':
                # print(postdata)
                user = EventParticipant.objects.get(pk=postdata['user'])
                user.share = postdata['num_guests']
                user.save()
            if postdata['submit'] == 'SetParticipants':
                # remove participants
                if len(postdata['remove_pks']) > 0:
                    for pk in postdata['remove_pks'].split(','):
                        user = User.objects.get(pk=pk)
                        Event.objects.get(pk=event_id).unset_participating(user)
                # add participants
                if len(postdata['add_pks']) > 0:
                    for pk in postdata['add_pks'].split(','):
                        user = User.objects.get(pk=pk)
                        Event.objects.get(pk=event_id).set_participating(user)

        return HttpResponseRedirect('/events/' + event_id + "/")
    else:
        userlist = User.objects.filter(is_active=True).prefetch_related("account")
        event = get_event_by_id(event_id)
        delta = (event.date.date() - datetime.datetime.today().date()).days
        event.days_until = delta

        expenseform = SetExpenseForm()
        expenseform.fields['user'].queryset = event.organizer_set

        guestform = SetGuestsForm()
        guestform.fields['user'].queryset = event.participant_set

        try:
            self_participant = event.participant_set.get(user=request.user)
        except EventParticipant.DoesNotExist:
            self_participant = False

        return render(request, 'event-details.html', {
            'event': event,
            'userlist': userlist,
            'self_participant': self_participant,
            'expenseform': expenseform,
            'guestform': guestform,
            'markdown': markdown,
            })
