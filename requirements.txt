django>=1.11,<2.0
jinja2
django-mptt>=0.8.6
psycopg2
markdown
sqlparse
pulp