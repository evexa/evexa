from evexa import settings
from django.apps import apps
from django.core import serializers
from django.db.models import Sum, Q, F
import gzip
from collections import OrderedDict
from finance.models import MoneyTransaction, Account, balancefield
from finance.queries import annotate_mt_context_long_form
from users.models import User
from datetime import datetime, timedelta, date, timezone, time
from decimal import Decimal
from django.template.loader import get_template


def iter_serialize_evexa(objects_chunk=500):
    app_dict = OrderedDict()
    app_list = [apps.get_app_config(app_label) for app_label in settings.evexa_apps]
    for conf in app_list:
        app_dict[conf] = [model for name, model in conf.models.items()]

    jsonifyer = serializers.get_serializer("json")()

    def gen_objects():
        models = serializers.sort_dependencies(app_dict.items())
        for model in models:
            qs = model.objects.all().order_by("pk")  # ordering to ensure we slice unique items
            count = qs.count()
            for page_start in range(0, count, objects_chunk):
                end = min(count, page_start + objects_chunk)
                yield qs[page_start:end]

    chunks = iter(gen_objects())

    def serialize(chunk):
        return jsonifyer.serialize(chunk)[1:-1]  # dont use first/last "[]"

    yield "[" + serialize(next(chunks))
    for chunk in chunks:
        yield "," + serialize(chunk)
    yield "]"


def gzip_iter_generator(iter, minimum_precompressed=2 ** 17):
    # 2**17 = 128kb precompressed
    saved_chunks = ""
    for chunk in iter:
        chunk = saved_chunks + chunk
        if len(chunk) < minimum_precompressed:
            saved_chunks = chunk
            continue
        saved_chunks = ""
        # Use low-level compression because it doesn't matter much
        yield gzip.compress(chunk.encode(), compresslevel=2)
    if saved_chunks != "":
        yield gzip.compress(saved_chunks.encode(), compresslevel=2)


def get_serialized_data_gzip(objects_chunk=500):
    yield from gzip_iter_generator(iter_serialize_evexa(objects_chunk=objects_chunk))


def balance_at_date_for_users(users, date, balance_name):
    return Account.objects.balance_at(
        date, to_field=balance_name
    ).filter(
        user_id__in=users.values("pk")
    ).values(
        "user_id",
        balance_name
    )


def get_summary_set(from_date, to_date):
    """
    Get a simple summary of transactions in a date interval.

    All transactions are aggregated in their respective context_long_form groups
    for the given period.

    for context_long_form in transactiontypes there is also the regular context_type to
    allow grouping the context_long_form by the actual transaction type.

    Returns: <QuerySet [{"context_long_form"    : "{transactiontypes}",
                         "context_type"         : "{transactiontype}",
                         "user_id"              : "{pk of user}",
                         "total"                :  Decimal()}
                         , ... ]>
    """
    # The transactions in the date interval
    transactions = MoneyTransaction.objects.filter(
        creation_date__gte=from_date,
        creation_date__lt=to_date,
    )

    # For each transaction type and account, get the total of transactions
    agg_values = annotate_mt_context_long_form(transactions).values(
        "context_long_form",
        "context_type",
        user_id=F('account_id')  # account_id is foreign key of account, which is OneToOne with user. Hence this is true
    ).order_by().annotate(
        total=Sum("balance_delta", output_field=balancefield())
    )

    return agg_values


def ensure_utc(timestamp):
    if not isinstance(timestamp, datetime) and isinstance(timestamp, date):
        timestamp = datetime.combine(timestamp, time())
    elif not issubclass(timestamp.__class__, datetime):
        raise TypeError("You must pass a datetime object.")
    if timestamp.tzinfo is None:
        timestamp = timestamp.replace(tzinfo=timezone.utc)
    return timestamp


def get_financial_active_users_at(values, date):
    users_with_transactions = User.objects.filter(account__pk__in=set(map(lambda x: x["user_id"], values)))

    users_with_non_zero_balance_at_start = User.objects.filter(
        account__pk__in=Account.objects.balance_at(date).filter(~Q(balance_at=0.00)).values("pk")
    )

    return users_with_transactions | users_with_non_zero_balance_at_start


def get_summary_as_latex(from_date=None, to_date=None, downloaded_by=None):
    """Get a simple summary for half-open date interval as latex table."""
    if all(d is None for d in (from_date, to_date)):
        from_date = datetime.now() - timedelta(28)
        to_date = datetime.now()
    elif from_date is None or to_date is None:
        raise ValueError("You must give either both or neither dates.")
    from_date = ensure_utc(from_date)
    to_date = ensure_utc(to_date)

    # Transactions aggregated in different contexts
    summary_values = list(get_summary_set(from_date, to_date))

    # Financial active users at the beginning of period as list
    financial_active_users = get_financial_active_users_at(summary_values, from_date)

    end_balance_string = "Slut balance"
    pre_balance_string = "Start balance"

    # Get pre and end balances
    pre_balance = balance_at_date_for_users(financial_active_users, from_date, pre_balance_string)
    end_balance = balance_at_date_for_users(financial_active_users, to_date, end_balance_string)

    # Convert queryset to list at this point and sort by name
    financial_active_users = list(financial_active_users.order_by('name').values("pk", "name"))

    # Find variable number of cols/context_long_form_types based on the transactions of period
    context_long_form_types = list(c2 for c1, c2 in
        sorted(
            set(map(lambda x: (x["context_type"], x["context_long_form"]), summary_values))
        )
    )

    # Add pre-balance, end-balance and the variable number of context-types to user with default value
    for user in financial_active_users:
        user.update(pre_balance.filter(user_id=user["pk"]).values(pre_balance_string)[:1][0])
        for lf_type in context_long_form_types:
            user[lf_type] = Decimal("0.00")
        user.update(end_balance.filter(user_id=user["pk"]).values(end_balance_string)[:1][0])

    # For keeping performance and still having readable code, we create a mapping dict from pk to list index
    pk_to_list_index = {}
    for i, user in enumerate(financial_active_users):
        pk_to_list_index[user["pk"]] = i

    # For each value of context type, we add the value to the active users
    for value in summary_values:
        user = financial_active_users[pk_to_list_index[value["user_id"]]]
        user[value["context_long_form"]] = value["total"]

    # Columns in the extraction excluding name
    cols = [pre_balance_string] + list(context_long_form_types) + [end_balance_string]

    # Delete pk for users/rows
    for user in financial_active_users:
        del user["pk"]

    template = get_template("simple_summary.tex", using="jinja2tex")

    return template.render(
        {
            "name_string": "Navn",
            "rows": financial_active_users,
            "cols": cols,
            "n_cols": len(cols) + 1,
            "from_date": from_date,
            "to_date": to_date,
            "user": downloaded_by,
        }
    )
