from django.forms import Field

from decimal import Decimal

TWOPLACES = Decimal("10")**-2

def String_Of_Csv(MultiField):
    """Wrap a field if instead of multiple form inputs, you have a single js-controlled field with
    comma-separated values being sent"""
    class MultiField(MultiField):
        def clean(self, value):
            if type(value) != str:
                value = value[0]
            return super().clean(value.split(","))
    return MultiField

class MultiDecimalField(Field):
    def clean(self, values):
        return [Decimal(v).quantize(TWOPLACES) for v in values]

MultiCsvDecimalField = String_Of_Csv(MultiDecimalField)

def label_from_instance(Field, attribute):
    class withLabel(Field):
        def label_from_instance(self, obj):
            return getattr(obj, attribute)
    return withLabel
