from jinja2 import Environment, Markup
from datetime import date, datetime
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.urlresolvers import reverse
from urllib import parse as urlparse
import re


def environment(**options):
    env = Environment(**options)
    env.globals.update({
       'static': staticfiles_storage.url,
       'url': reverse,
       'today': date.today,
       "now": datetime.now,
       "get_encode": get_parameters_encode,
    })
    return env


def get_parameters_encode(params, **update):
    # understand "none" to mean remove it from parameters
    parameters = {**params, **update}
    for k, v in parameters.items():
        if v is None:
            del parameters[k]
    return urlparse.urlencode(parameters)


LATEX_SUB = (
    # See http://flask.pocoo.org/snippets/55/
    (re.compile(r'\\'), r'\\textbackslash'),
    (re.compile(r'([{}_#%&$])'), r'\\\1'),
    (re.compile(r'~'), r'\~{}'),
    (re.compile(r'\^'), r'\^{}'),
    (re.compile(r'"'), r"''"),
    (re.compile(r'\.\.\.+'), r'\\ldots'),
)

def tex_escape(value):
    for pattern, substitute in LATEX_SUB:
        value = pattern.sub(substitute, value)
    return value

def latex_environment(**options):
    options.update({
        "autoescape": False,
        "block_start_string": r"\block{",
        "block_end_string": "}",
        "variable_start_string": r"\var{",
        "variable_end_string": "}",
        "line_statement_prefix": "%$",
        "line_comment_prefix": "%&",
        "trim_blocks": True
    })
    env = Environment(**options)
    env.globals.update({
        "today": date.today,
        "now": datetime.now,
    })
    env.filters["e"] = tex_escape
    return env
