from datetime import datetime


class SimpleLogger:

    info_messages = []
    warning_messages = []
    failure_messages = []

    def info(self, message):
        self.info_messages.append((datetime.now(), message))

    def warning(self, message):
        self.warning_messages.append((datetime.now(), message))

    def failure(self, message):
        self.failure_messages.append((datetime.now(), message))

    def logs_to_list(self):
        return self.info_messages + self.warning_messages + self.failure_messages