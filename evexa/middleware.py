from evexa import settings
from django.urls import reverse


def navbar(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        navbar = [{
            "icon": icon,
            "name": name,
            "href": reverse(url),
            } for icon, name, url in settings.navbar]
        navbar += [{
             "icon": "" or icon,
             "name": name,
             "href": url
             } for icon, name, url in settings.navbar_static]
        request.navbar = navbar

        request.community_name = settings.COMMUNITY_NAME

        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    return middleware
