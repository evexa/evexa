from django.contrib import admin
from finance.models import Account, MoneyTransaction
# Register your models here.
admin.site.register(Account)
admin.site.register(MoneyTransaction)
