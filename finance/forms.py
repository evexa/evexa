from django import forms
from evexa.formtools import MultiCsvDecimalField, label_from_instance
from evexa.export_tools import get_serialized_data_gzip, get_summary_as_latex
from django.http import StreamingHttpResponse, HttpResponse
from .models import Transaction
from users.models import User
from django.db import transaction
from django.db.models import Q, F, Value, CharField
from django.db.models.functions import Concat

finance_active_users_set = User.objects.filter(Q(is_active=True) | ~Q(account__balance=0)
    ).annotate(name_with_balance=Concat(F("name"), Value(" ("), F("account__balance"),
                                        Value(" kr)"), output_field=CharField())
               ).prefetch_related("account")


class CreateTransactionsForm(forms.Form):
    kind = forms.ChoiceField(choices=tuple(Transaction.kind_choices.items()))
    comment = forms.CharField(required=False)
    users = label_from_instance(forms.ModelMultipleChoiceField, "name_with_balance")(
            queryset=finance_active_users_set
        )
    deltas = MultiCsvDecimalField()

    def clean(self):
        data = super().clean()
        if len(data["deltas"]) != len(data["users"]):
            raise forms.ValidationError("There was an ueven number of users and balances."
            " Deltas: {}, Users: {}".format(len(data["deltas"]), len(data["users"])))
        return data

    def make_transactions(self):
        data = self.cleaned_data
        with transaction.atomic():
            for u, delta in zip(data["users"], data["deltas"]):
                Transaction(account=u.account, balance_delta=delta,
                            kind=data["kind"], comment=data["comment"]).save()


class ExportForm(forms.Form):
    download_choices = ["all.json.gz", "simplesummary"]
    Download = forms.ChoiceField(choices=[(d, d) for d in download_choices])
    date_from = forms.DateTimeField(required=False, input_formats=('%d/%m/%Y %H:%M:%S',))
    date_to = forms.DateTimeField(required=False, input_formats=('%d/%m/%Y %H:%M:%S',))

    def getfile(self, user=None):
        data = self.cleaned_data
        if data["Download"] == "simplesummary":
            # raise NotImplementedError()
            latex_string = get_summary_as_latex(data["date_from"], data["date_to"], user)
            response = HttpResponse(latex_string)
            response['Content-Disposition'] = "attachment; filename={}".format("evexa_simplesummary.tex")
            return response
        elif data["Download"] == "all.json.gz":
            data = get_serialized_data_gzip()
            response = StreamingHttpResponse(data, content_type="application/json+gzip")
            response['Content-Disposition'] = "attachment; filename={}".format("evexa_total_export.json.gz")
            return response
