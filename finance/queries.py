""" Just some long queries. """
from django.db.models import Case, When, CharField
from evexa import settings
from django.apps import apps


def _get_moneytransaction_subclasses():
    from .models import MoneyTransaction  # to avoid circular dependencies
    app_list = (apps.get_app_config(app_label) for app_label in settings.evexa_apps)
    models = (model for conf in app_list for name, model in conf.models.items())
    mt_subclasses = filter(lambda model: issubclass(model, MoneyTransaction)
                           and model is not MoneyTransaction, models)
    return list(mt_subclasses)


def annotate_mt_context_long_form(queryset):
    from .models import MoneyTransaction  # to avoid circular dependencies
    qs = queryset.annotate(context_long_form=Case(
        *[When(context_type=TransClass.__name__.lower(),
               then=TransClass.get_context_long_form())
          for TransClass in _get_moneytransaction_subclasses()],
        default=MoneyTransaction.get_context_long_form(),
        output_field=CharField()
    ))
    return qs
