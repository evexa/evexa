from django.db import models, transaction
from django.conf import settings
from django.db.models import Q, F, Value, CharField, Case, When, OuterRef, Subquery, Sum
from django.db.models.functions import Coalesce
from datetime import datetime, date, time, timezone
from decimal import Decimal


def balancefield():
    return models.DecimalField(default=0, decimal_places=2, max_digits=8)


class AccountManager(models.Manager):
    def balance_at(self, date_time, to_field="balance_at"):
        """Get the balance at a historic time.

        It subtracts all transactions since then from the current balance."""
        if not isinstance(date_time, datetime) and isinstance(date_time, date):
            date_time = datetime.combine(date_time, time())
        elif not issubclass(date_time.__class__, datetime):
            raise TypeError("You must pass a datetime object.")
        if date_time.tzinfo is None:
            date_time = date_time.replace(tzinfo=timezone.utc)

        transactions = MoneyTransaction.objects.filter(creation_date__gte=date_time)
        transactions_relevant = transactions.filter(account__pk=OuterRef("pk"))
        agg = transactions_relevant.values("account").order_by().annotate(
            d=Coalesce(
                Sum("balance_delta", output_field=balancefield()),
                Value(0)
                )
            )

        deltas = Subquery(agg.values("d")[:1], output_field=balancefield())
        return self.annotate(**{to_field: F("balance") - Coalesce(deltas, Value(Decimal("0.00")))})


class Account(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True)
    balance = models.DecimalField(default=0, decimal_places=2, max_digits=8)

    objects = AccountManager()

    def recalculate_balance(self):
        return MoneyTransaction.objects.filter(account=self).aggregate(
            balance=models.Sum("balance_delta"))["balance"]

    def valid_balance(self):
        return self.recalculate_balance() == self.balance


class MoneyTransaction(models.Model):
    # Transactions not only belong to an account but is the history of the community.
    # Therefore dont delete them :)
    account = models.ForeignKey(Account, null=True, on_delete=models.SET_NULL)
    balance_delta = models.DecimalField(decimal_places=2, max_digits=8)
    creation_date = models.DateTimeField(auto_now=True)
    context_type = models.CharField(max_length=32)
    _context_long_form = None

    def __init__(self, *args, **kwargs):
        if "context_type" not in kwargs:
            kwargs["context_type"] = str(self.__class__.__name__).lower()
        super().__init__(*args, **kwargs)

    @property
    def context(self):
        """Get this object as the class that subclassed MoneyTransaction."""
        return getattr(self, self.context_type)

    def __str__(self):
        # Override this in subclasses
        return "MoneyTransaction ({})".format(self.context_type)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.account.balance += self.balance_delta
            self.account.save()
            return super(MoneyTransaction, self).save(*args, **kwargs)

    @classmethod
    def get_context_long_form(cls):
        """For use in queries."""
        if cls._context_long_form is None:
            return F("context_type")
        else:
            return Value(cls._context_long_form)


class Transaction(MoneyTransaction):
    CASH = "C"
    FEE = "F"
    FINE = "X"
    OTHER = "O"
    kind_choices = dict(((CASH, "Cash"), (FEE, "Fee"), (FINE, "Fine"), (OTHER, "Other")))
    ctx_description = dict(((CASH, "Cash"), (FEE, "Fees"), (FINE, "Fines"), (OTHER, "Other")))
    kind = models.CharField(
        choices=tuple(kind_choices.items()), max_length=1)
    comment = models.CharField(max_length=80, blank=True)

    def __str__(self):
        s = "{} transaction".format(self.get_kind_display())
        if self.comment:
            s += " ({})".format(self.comment)
        return s

    @classmethod
    def get_context_long_form(cls):
        qs = cls.objects.filter(moneytransaction_ptr_id=OuterRef("pk")).order_by()
        kind_case = Case(
            *[When(Q(kind=k), Value(v))
              for k, v in cls.ctx_description.items()],
            default=Value("Unknown"),
            output_field=CharField()
        )
        return Subquery(qs.annotate(longform=  # Concat(
                                    kind_case,  # Value(" transaction"),
                                    # output_field=CharField()
                                    ).values("longform")[:1])
