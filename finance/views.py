from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from .models import MoneyTransaction
from .forms import CreateTransactionsForm, finance_active_users_set, ExportForm
from .queries import _get_moneytransaction_subclasses

import math

FINANCE_SUBNAV = {
    # ("Name", "req_admin"): "url",
    ("List", False): "finance:index",
    ("Create", True): "finance:create",
    ("Export", False): "finance:export",
}

@login_required
def list_transactions(request):
    page = int(request.GET.get('p', 1))
    page_size = 25
    page_start = (page - 1) * page_size
    page_end = page_start + page_size
    page_trunc_limit = 10

    related = ["account"]
    related += [tClass.__name__.lower() for tClass in _get_moneytransaction_subclasses()]
    ts = MoneyTransaction.objects.order_by("-creation_date").select_related(*related)
    if "user" in request.GET:
        user_filter = request.GET["user"]
        ts = ts.filter(account__user__pk=int(user_filter))
    else:
        user_filter = None

    N_ts = ts.count()
    if N_ts == 0:
        ts = []
    items_page = ts[page_start:page_end]
    num_pages = math.ceil(N_ts / int(page_size))

    pages = []

    if num_pages >= page_trunc_limit:
        pages = [p for p in range(page-2, page+3) if p > 0 and p <=num_pages]

        if page > 3:
            pages = [1,"."] + pages

        if page < num_pages-4:
            pages = pages + [".", num_pages]
    else:
        pages = [p for p in range(num_pages)]

    return render(request, "transactionlist.html", {
        "transactions":items_page,
        'pages': pages,
        'num_pages': num_pages,
        'at_page': page,
        "subnav": FINANCE_SUBNAV
        })

@login_required
def create(request):
    if request.method == "POST":
        form = CreateTransactionsForm(request.POST)
        if request.user.is_staff and form.is_valid():
            form.make_transactions()
    form = CreateTransactionsForm()

    return render(request, "create_transactions.html", {
        "form": form,
        "users": finance_active_users_set[::1],
        "subnav": FINANCE_SUBNAV,
    })

@login_required
def export(request):
    if request.method == "POST":
        form = ExportForm(request.POST)
        if form.is_valid():
            return form.getfile(request.user)
    form = ExportForm()
    return render(request, "export.html", {
        "form": form,
        "subnav": FINANCE_SUBNAV
    })
