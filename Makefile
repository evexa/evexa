all: serve

deps:
	pip3 install -r requirements.txt

serve:
	./manage.py runserver

init:
	echo "Warning: Please be aware that the makefile init has changed. It no longer creates migrations, which you should do manually if you changed a model."
	./manage.py migrate
	./manage.py createsuperuser

load_example_expensetypes:
	./manage.py loaddata example_k1200_expensetypes.json
